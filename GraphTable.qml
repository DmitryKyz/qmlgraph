import QtQuick 2.15
import Qt.labs.qmlmodels 1.0
import QtQuick.Controls 2.15

Rectangle
{
    property alias model: tableView.model
    property alias delegate: tableView.delegate

    property alias button: btn
    property alias tableView: tableView

    Button
    {
        id: btn

        width: verticalHeader.width
        height: horizontalHeader.height
    }

    TableView
    {
        id: tableView

        anchors.left: verticalHeader.right
        anchors.top:  horizontalHeader.bottom
        width: parent.width - verticalHeader.width
        height: parent.height -  horizontalHeader.height

        columnSpacing: 1
        rowSpacing: 1

        ScrollBar.horizontal: ScrollBar{}
        ScrollBar.vertical: ScrollBar{}

        clip: true
    }

    HorizontalHeaderView {
        id:  horizontalHeader
        syncView:  tableView
        anchors.left: tableView.left
        anchors.top: parent.top

    }

    VerticalHeaderView {
        id:  verticalHeader
        syncView:  tableView
        anchors.left: parent.left
        anchors.top: tableView.top

        clip: true
    }
}
