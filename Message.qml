import QtQuick 2.0
import QtQuick.Dialogs 1.2
import QtQuick.Layouts 1.3


Dialog
{
    id: root
    title: qsTr("Ошибка")

    property alias text: message.text
    property alias icon: image.source
    property alias font: message.font

    RowLayout
    {
        Image {
            id: image
            source: "icons/Error.png"

            Layout.alignment: Qt.AlignLeft
        }

        Text {
            id: message
            font.pixelSize: 12
            Layout.preferredWidth: contentWidth

            onContentWidthChanged: root.width = contentWidth + image.width + 30
        }
    }

    standardButtons: StandardButton.Ok
}
