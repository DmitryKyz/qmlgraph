import QtQuick 2.15
import QtQuick.Controls 2.15

Item {
    id: root

    property alias editor: spinBox
    property alias displayer: rect
    property alias displayText: text

    property var inModelFunc: function (value) {model.display = value} // func(value) - функция, которая передает данные в модель
    property bool editable: true
    property var displayValue: display


    implicitHeight: 40
    implicitWidth: 40

    state: ""
    states: [
        State{
            name: "editing"
            PropertyChanges {target: spinBox; z: 1}
            PropertyChanges {target: rect; z: 0}
            PropertyChanges {target: spinBox; focus: true}
        }
    ]

    SpinBox
    {
        id: spinBox
        anchors.fill: parent
        font.pointSize: 12
        focusPolicy: Qt.ClickFocus
        editable: true

        value: displayValue

        Keys.onPressed: {
            if (event.key == Qt.Key_Return) {
                sendValueInModel()
                event.accepted = true
            }
        }

        onFocusChanged: {
            if (focus == false)
                sendValueInModel()
        }

        function sendValueInModel()
        {
            inModelFunc(value)
            parent.state = ""
        }
    }

    Rectangle
    {
        id: rect
        anchors.fill: parent
        border.color: "lightGray"
        color: !editable ? "red" : "lightGray"

        Text {
            id: text
            anchors.fill: parent

            text: displayValue
            horizontalAlignment: Text.AlignHCenter
            verticalAlignment: Text.AlignVCenter
            font.pointSize: 14

        }
    }

    MouseArea
    {
        anchors.fill: parent
        onClicked: if (editable) parent.state = "editing"
    }
}

