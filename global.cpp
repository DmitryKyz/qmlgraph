#include "global.h"

/*
 * Возвращает название вершины.
 */
QString getVertexName(int vertexIndex)
{
    int charCode = 'A';

    if (vertexIndex > 25)
    {
        int index = vertexIndex / 26;
        int newVertexIndex = vertexIndex % 26;
        return QChar(charCode + newVertexIndex) + QString::number(index);
    }

    return QChar(charCode + vertexIndex);
}
