#ifndef GLOBAL_H
#define GLOBAL_H

#include <QString>

constexpr int MATRIX_MIN_SIZE = 2;

QString getVertexName(int vertexIndex);

#endif // GLOBAL_H
