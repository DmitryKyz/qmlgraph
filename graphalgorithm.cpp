#include "graphalgorithm.h"
#include "graphview.h"
#include "graphvertex.h"
#include "graphrib.h"
#include "ribdata.h"
#include <stdlib.h>

GraphAlgorithm::GraphAlgorithm(QObject *parent) : QObject(parent)
{

}

/*
 * Находит минимальное остовное дерево по алгоритму Краскала
 * Устанавливает флаг "use" у вершин и ребер, которые входят в дерево
 *
 * Возвращает результат с ключами:
 *  - ["ok"] - найдено ли дерево;
    - ["weight"] - вес дерева;
    - ["message"] - строка результата.
 */
QVariantMap GraphAlgorithm::processMinimalSpanningTree()
{
    reset();
    std::sort(m_ribs.begin(), m_ribs.end(), [](auto &a, auto &b){return a->weight() < b->weight();});

    QVector<int> treeId(m_vertex.count()); // индексы деревьев
    for (int i = 0; i < treeId.count(); i++)
        treeId[i] = i;

    int weight = 0;
    for (int i = 0; i < m_ribs.count(); i++)
    {
        int start = m_ribs[i]->beginVertex()->num();
        int end = m_ribs[i]->endVertex()->num();

        // если деревья не объединены
        if (treeId[start] != treeId[end])
        {
            m_ribs[i]->setUse(true); // включение ребра в МОД
            weight += m_ribs[i]->weight();

            int oldId = treeId[end];
            int newId = treeId[start];

            // объединение деревьев
            for (int j = 0; j < m_vertex.count(); ++j)
                if (treeId[j] == oldId)
                    treeId[j] = newId;
        }
    }

    // проверка успешного составления дерева
    int index = treeId[0];
    for (int i = 1; i < treeId.count(); i++)
        if (index != treeId[i])
        {
            reset();
            return createResult(false, 0, tr("Граф не является связным!"));
        }

    // окрашивание вершин
    for (auto v : m_vertex)
        v->setUse(true);

    return createResult(true, weight, tr("Вес минимального остовного дерева равен %1").arg(weight));
}

/*
 * Находит минимальный путь между двумя вершинами по алгоритму Дейкстры
 * Устанавливает флаг "use" у вершин и ребер, которые входят в путь
 *
 * Возвращает результат с ключами:
 *  - ["ok"] - найден ли путь;
    - ["weight"] - вес пути;
    - ["message"] - строка результата.
 */
QVariantMap GraphAlgorithm::processMinimalPath(QString startName, QString endName)
{
    reset();

    auto it = std::find_if(m_vertex.begin(), m_vertex.end(), [startName](auto &v){return v->name() == startName;});
    int start = (*it)->num();

    it = std::find_if(m_vertex.begin(), m_vertex.end(), [endName](auto &v){return v->name() == endName;});
    int end = (*it)->num();

    if (start < 0 || end < 0)
        return createResult(false, 0, tr("Переданные вершины не найдены!"));

    QList<int> path = createPath(start, end);

    if (path.isEmpty())
        return createResult(false, 0, tr("Путь не существует!"));

    // окрашивание вершин
    for (auto v : path)
        m_vertex[v]->setUse(true);

    int weight = 0;

    // окрашивание ребер в пути
    for (int i = 1; i < path.count(); i++)
    {
        int v1 = path[i-1];
        int v2 = path[i];

        for (int j = 0; j < m_ribs.count(); j++)
        {
            if ((m_ribs[j]->beginVertex()->num() == v1 && m_ribs[j]->endVertex()->num() == v2) ||
                (m_ribs[j]->endVertex()->num() == v1 && m_ribs[j]->beginVertex()->num() == v2))
            {
                m_ribs[j]->setUse(true);
                weight += m_ribs[j]->weight();
                break;
            }
        }
    }

    // создание результата
    QStringList vertexInPath;
    for (auto p : path)
        vertexInPath.append(m_vertex[p]->name());

    QString message = tr("Расстояние между вершинами %1\nПуть: %2").arg(weight).arg(vertexInPath.join("-"));
    auto result = createResult(false, weight, message);
    result["path"] = vertexInPath;

    return result;
}

/*
 * возвращает список всех ребер, выходящих из каждой вершины
 */
QList<RibData> GraphAlgorithm::createGraph()
{
    QList<RibData> graph;

    for (auto rib : m_ribs)
        graph.append({rib->beginVertex()->num(), rib->endVertex()->num(), rib->weight()});

    // добавление недостающих ребер
    for (auto rib : m_ribs)
        graph.append({rib->endVertex()->num(), rib->beginVertex()->num(), rib->weight()});

    return graph;
}

/*
 * возвращает кратчайший путь от вершины start к вершине end
 *
 * путь состоит из номеров вершин
 * start, end - номера вершин, между которыми нужно найти путь
 */
QList<int> GraphAlgorithm::createPath(int start, int end)
{
    QList<RibData> graph = createGraph(); // список всех ребер, выходящих из каждой вершины
    QVector<int> length(m_vertex.count(), INT_MAX); // длины путей от конечной точки ко всем остальным
    QVector<int> parentV(m_vertex.count()); // вершины родители
    QVector<bool> usedV(m_vertex.count()); // используемые вершины

    length[end] = 0;

    // поиск длин пути от конечной вершины ко всем остальным
    for (int i = 0; i < m_vertex.count(); i++)
    {
        int v = -1;
        for (int j = 0; j < m_vertex.count(); j++)
            if (!usedV[j] && (v == -1 || length[j] < length[v]))
                v = j;

        // если просмотр вершины без ребер
        if (length[v] == INT_MAX)
            break;

        usedV[v] = true;

        for (int j = 0; j < graph.count(); j++)
        {
            if (graph[j].start != v)
                continue;

            int to = graph[j].end;
            int len = graph[j].weight;

            if (length[v] + len < length[to])
            {
                length[to] = length[v] + len;
                parentV[to] = v;
            }
        }
    }

    // проверка нахождения пути
    if (length[start] == INT_MAX)
        return {};

    // восстановление пути
    QList<int> path;
    for (int v = start; v != end; v = parentV[v])
        path.append(v);
    path.append(end);
    return path;
}

QVariantMap GraphAlgorithm::createResult(bool ok, int weight, const QString &message)
{
    QVariantMap result;
    result["ok"] = ok;
    result["weight"] = weight;
    result["message"] = message;

    return result;
}

void GraphAlgorithm::setRibs(QList<QSharedPointer<GraphRib> > ribs)
{
    m_ribs = ribs;
    reset();
}

void GraphAlgorithm::setVertex(QVector<QSharedPointer<GraphVertex> > vertex)
{
    m_vertex = vertex;
    reset();
}

/*
 * Сброс флага "use" у вершин и ребер
 */
void GraphAlgorithm::reset()
{
    for (auto rib : m_ribs)
        rib->setUse(false);

    for (auto v : m_vertex)
        v->setUse(false);
}
