#ifndef GRAPHALGORITHM_H
#define GRAPHALGORITHM_H

#include <QObject>
#include <QVariantMap>
#include <QQuickItem>

class GraphRib;
class GraphVertex;
class GraphView;
struct RibData;

/*
 * Класс с алгоритмами над графом
 * позволяет найти минимальное остовное дерево графа и минимальный путь между двумя точками
 * для работы должен быть передан в GraphView
 */
class GraphAlgorithm : public QObject
{
    Q_OBJECT
    QML_ELEMENT

public:
    explicit GraphAlgorithm(QObject *parent = nullptr);

    void setRibs(QList<QSharedPointer<GraphRib>> ribs);
    void setVertex(QVector<QSharedPointer<GraphVertex>> vertex);

    Q_INVOKABLE QVariantMap processMinimalSpanningTree();
    Q_INVOKABLE QVariantMap processMinimalPath(QString startName, QString endName);
    Q_INVOKABLE void reset();

    QVariantMap createResult(bool ok, int weight, const QString &message);

signals:
    void resultChanged();

private:
    QList<QSharedPointer<GraphRib>> m_ribs; // список ребер графа
    QVector<QSharedPointer<GraphVertex>> m_vertex; // вектор вершин графа

    QList<RibData> createGraph();
    QList<int> createPath(int start, int end);
};

#endif // GRAPHALGORITHM_H
