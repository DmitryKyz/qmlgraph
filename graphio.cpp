#include "graphio.h"
#include <QTextStream>
#include <QFile>
#include <QDebug>

GraphIO::GraphIO(QObject *parent) : QObject(parent)
{

}

void GraphIO::setSource(QUrl &source)
{
    if (m_source != source)
    {
        m_source = source;
        sourceChanged();
    }
}

void GraphIO::setMatrix(QVector<QVector<uint>> &matrix)
{
    if (m_matrix != matrix)
    {
        m_matrix = matrix;
        matrixChanged();
    }
}

void GraphIO::setError(QString err)
{
    m_error = err;
    if (!err.isEmpty())
        emit errorChanged();
}

QString GraphIO::fromUrl(QUrl url)
{
    return url.toLocalFile();
}

/*
 * сохраняет матрицу смежности
 */

void GraphIO::save()
{
    setError("");

    QFile file(m_source.toLocalFile());
    file.open(QIODevice::WriteOnly);

    if (!file.isOpen())
    {
        setError(tr("Не удалось сохранить файл по указанному пути!"));
        return;
    }

    QTextStream out(&file);
    QString string = "";

    for (int i = 0; i < m_matrix.count(); i++)
    {
        for (int j = 0; j < m_matrix.count(); j++)
            string += QString("%1, ").arg(m_matrix[i][j]);
        string += "\n";
    }

    string.remove(string.length() - 3, 3);
    out << string;

    emit saved();
}

/*
 * загружает матрицу смежности
 */

void GraphIO::load()
{
    setError("");

    QString line;
    QVector<QVector<uint>> matrix;

    QFile file(m_source.toLocalFile());
    file.open(QIODevice::ReadOnly);

    if (!file.isOpen())
    {
        setError(tr("Не удалось загрузить файл из указанного пути!"));
        return;
    }

    QTextStream in(&file);

    int size = 0;
    while (!in.atEnd())
    {
        line = in.readLine();
        line = line.trimmed();

        QStringList rowStr = line.split(",");

        if (rowStr.last().isEmpty())
            rowStr.removeLast();

        if (size == 0)
            size = rowStr.count();

        if (size != rowStr.count())
        {
            setError(tr("Загружаемая матрица не квадратная!"));
            return;
        }

        QVector<uint> row;
        for (int i = 0; i < size; i++)
        {
            bool ok;
            uint item = rowStr[i].toUInt(&ok);

            if (!ok)
            {
                setError(tr("Загружаемая матрица содержит не допустимые символы или отрицательные числа!"));
                return;
            }

            row.append(item);
        }
        matrix.append(row);
    }

    if (size != matrix.count())
    {
        setError(tr("В загружаемой матрице количество строк не равно количеству столбцов!"));
        return;
    }

    for (int i = 0; i < matrix.count(); i++)
        for (int j = 0; j < matrix.count(); j++)
        {
            if (i == j && matrix[i][j] != 0)
            {
                setError(tr("Матрица смежности не должна содержать петли"));
                return;
            }

            if (matrix[i][j] != matrix[j][i])
            {
                setError(tr("Матрица должна задавать неориентированный граф!"));
                return;
            }
        }

    m_matrix = matrix;

    emit matrixChanged();
    emit loaded();
}
