#ifndef GRAPHIO_H
#define GRAPHIO_H

#include <QObject>
#include <QUrl>
#include <QQuickItem>

/*
 * Класс для загрузки и сохранения матрицы смежности в файл .mtx
 */
class GraphIO : public QObject
{
    Q_OBJECT
    QML_ELEMENT

    Q_PROPERTY(QVector<QVector<uint>> matrix READ matrix WRITE setMatrix NOTIFY matrixChanged)
    Q_PROPERTY(QUrl source READ source WRITE setSource NOTIFY sourceChanged)
    Q_PROPERTY(QString error READ error NOTIFY errorChanged)

public:
    explicit GraphIO(QObject *parent = nullptr);

    QUrl source() {return m_source;}
    QVector<QVector<uint>> matrix() {return  m_matrix;}
    QString error() {return m_error;}

    void setSource(QUrl &source);
    void setMatrix(QVector<QVector<uint>> &matrix);

    Q_INVOKABLE void save();
    Q_INVOKABLE void load();
    Q_INVOKABLE QString fromUrl(QUrl url);

signals:
    void matrixChanged();
    void sourceChanged();
    void errorChanged();
    void loaded();
    void saved();

private:
    QUrl m_source; // путь к файлу
    QVector<QVector<uint>> m_matrix; // матрица смежности
    QString m_error; // ошибка при загрузке/сохранении

    void setError(QString err);
};

#endif // GRAPHIO_H
