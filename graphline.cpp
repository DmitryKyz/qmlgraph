#include "graphline.h"
#include <QSGNode>
#include <QSGFlatColorMaterial>
#include <QSGGeometry>

GraphLine::GraphLine(QQuickItem *parent):
    QQuickItem(parent)
{
    setFlag(ItemHasContents, true);
}

void GraphLine::setColor(QColor &color)
{
    if (color != m_color)
    {
        m_color = color;
        dirty.set(static_cast<int>(DirtyPr::Color));

        emit colorChanged();
        update();
    }
}

void GraphLine::setLineWidth(int width)
{
    if (m_lineWidth != width)
    {
        m_lineWidth = width;
        dirty.set(static_cast<int>(DirtyPr::LineWidth));
        emit lineWidthChanged();
        update();
    }
}

void GraphLine::setX1(qreal x1)
{
    if (m_x1 != x1)
    {
        m_x1 = x1;
        setX(x1);
        setWidth(m_x2 - m_x1);
        emit x1Changed();
    }
}

void GraphLine::setY1(qreal y1)
{
    if (m_y1 != y1)
    {
        m_y1 = y1;
        setY(y1);
        setHeight(m_y2 - m_y1);
        emit y1Changed();
    }
}

void GraphLine::setX2(qreal x2)
{
    if (m_x2 != x2)
    {
        m_x2 = x2;
        setWidth(m_x2 - m_x1);
        emit x2Changed();
    }
}

void GraphLine::setY2(qreal y2)
{
    if (m_y2 != y2)
    {
        m_y2 = y2;
        setHeight(m_y2 - m_y1);
        emit y2Changed();
    }
}

QSGNode *GraphLine::updatePaintNode(QSGNode *oldNode, UpdatePaintNodeData *)
{
    QSGGeometryNode *node = nullptr;
    QSGGeometry *geometry = nullptr;

    if (!oldNode) {
        node = new QSGGeometryNode;

        geometry = new QSGGeometry(QSGGeometry::defaultAttributes_Point2D(), 2);
        geometry->setLineWidth(m_lineWidth);
        geometry->setDrawingMode(QSGGeometry::DrawLines);
        node->setGeometry(geometry);
        node->setFlag(QSGNode::OwnsGeometry);

        QSGFlatColorMaterial *material = new QSGFlatColorMaterial;
        material->setColor(m_color);

        node->setMaterial(material);
        node->setFlags(QSGNode::OwnsMaterial | QSGNode::OwnsGeometry);

        dirty.reset();
    } else {
        node = static_cast<QSGGeometryNode *>(oldNode);
        geometry = node->geometry();
        geometry->allocate(2);

        if (dirty.test(static_cast<int>(DirtyPr::LineWidth)))
            geometry->setLineWidth(m_lineWidth);

        if (dirty.test(static_cast<int>(DirtyPr::Color)))
            static_cast<QSGFlatColorMaterial *>(node->material())->setColor(m_color);

        dirty.reset();
    }

    QSGGeometry::Point2D *vertices = geometry->vertexDataAsPoint2D();
    vertices[0].set(0, 0);
    vertices[1].set(width(), height());

    node->markDirty(QSGNode::DirtyGeometry);
    return node;
}
