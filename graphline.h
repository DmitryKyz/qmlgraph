#ifndef LINE_H
#define LINE_H

#include <QObject>
#include <QQuickItem>
#include <bitset>

/*
 * Класс QML-элемента линии
 */

class GraphLine : public QQuickItem
{
    Q_OBJECT
    QML_ELEMENT

    Q_PROPERTY(QColor color READ color WRITE setColor NOTIFY colorChanged)
    Q_PROPERTY(int lineWidth READ lineWidth WRITE setLineWidth NOTIFY lineWidthChanged)
    Q_PROPERTY(qreal x1 READ x1 WRITE setX1 NOTIFY x1Changed)
    Q_PROPERTY(qreal y1 READ y1 WRITE setY1 NOTIFY y1Changed)
    Q_PROPERTY(qreal x2 READ x2 WRITE setX2 NOTIFY x2Changed)
    Q_PROPERTY(qreal y2 READ y2 WRITE setY2 NOTIFY y2Changed)

    enum class DirtyPr
    {
        LineWidth,
        Color
    };

public:
    explicit GraphLine(QQuickItem *parent = nullptr);

    QColor color() const {return m_color;}
    int lineWidth() const {return m_lineWidth;}
    qreal x1() const {return m_x1;}
    qreal y1() const {return m_y1;}
    qreal x2() const {return m_x2;}
    qreal y2() const {return m_y2;}

    void setColor(QColor &color);
    void setLineWidth(int width);
    void setX1(qreal x2);
    void setY1(qreal y2);
    void setX2(qreal x2);
    void setY2(qreal y2);

protected:
    QSGNode *updatePaintNode(QSGNode *oldNode, UpdatePaintNodeData *) override;

signals:
    void colorChanged();
    void lineWidthChanged();
    void x1Changed();
    void y1Changed();
    void x2Changed();
    void y2Changed();

private:
    int m_lineWidth = 1;
    QColor m_color;

    qreal m_x1 = 0;
    qreal m_y1 = 0;
    qreal m_x2 = 0;
    qreal m_y2 = 0;

    std::bitset<2> dirty; // какие свойства были изменены

};

#endif // LINE_H
