#include "graphmatrixmodel.h"
#include <QBrush>
#include <QDebug>
#include "global.h"

GraphMatrixModel::GraphMatrixModel(QObject *parent) :
    QAbstractTableModel(parent)
{
    createMatrix(4);
}

/*
 * возвращает флаги возможных действий над ячейками таблицы
 * index - адрес ячейки
 */
Qt::ItemFlags GraphMatrixModel::flags(const QModelIndex &index) const
{
    Qt::ItemFlags theFlags = QAbstractTableModel::flags(index);
    if (!index.isValid() || isNotEditable(index))
        return theFlags;

    theFlags |= Qt::ItemIsEditable;

    return theFlags;
}

QHash<int, QByteArray> GraphMatrixModel::roleNames() const
{
    auto roles = QAbstractTableModel::roleNames();
    roles[NULL_ROLE] = "isNull";
    return roles;
}

/*
 * устанавливает размер матрицы смежности
 * size - новый размер матрицы
 */
void GraphMatrixModel::setMatrixSize(int size)
{
    if (size < MATRIX_MIN_SIZE)
        return;

    graphMatrix.resize(size);
    for (int i = 0; i < graphMatrix.count(); i++)
        graphMatrix[i].resize(size);

    beginResetModel();
    endResetModel();
    matrixChanged();
}

void GraphMatrixModel::setMatrix(QVector<QVector<uint>> matrix)
{
    if (matrix != graphMatrix)
    {
        beginResetModel();
        graphMatrix = matrix;
        endResetModel();

        matrixChanged();
    }
}

int GraphMatrixModel::minSize()
{
    return MATRIX_MIN_SIZE;
}

/*
 * возвращает данные из ячейки
 * index - адрес ячейки
 * role - тип данных ячейки
 */
QVariant GraphMatrixModel::data(const QModelIndex &index, int role) const
{
    if (!checkIndex(index))
        return QVariant();

    if (role == Qt::DisplayRole || role == Qt::EditRole)
        return graphMatrix[index.row()][index.column()];

    if (role == NULL_ROLE)
        return isNotEditable(index);

    return QVariant();
}

bool GraphMatrixModel::isNotEditable(const QModelIndex &index) const
{
    return index.row() == index.column();
}

/*
 * возвращает название заголовка
 * section - номер заголовка
 * orientation - ориентация заголовка (заголовок строки/столбца)
 * role - тип данных заголовка
 */
QVariant GraphMatrixModel::headerData(int section, Qt::Orientation orientation, int role) const
{
    Q_UNUSED(orientation);

    if (role != Qt::DisplayRole)
        return QVariant();

    return getVertexName(section);
}

/*
 * возвращает количество строк в таблице
 * index - адрес ячейки
 */
int GraphMatrixModel::rowCount(const QModelIndex &parent) const
{
    if (parent.isValid())
        return 0;

    return graphMatrix.count();
}

/*
 * возвращает количество столбцов в таблице
 * index - адрес ячейки
 */
int GraphMatrixModel::columnCount(const QModelIndex &parent) const
{
    return rowCount(parent);
}

/*
 * заносит данные в ячейку
 * возвращает true в случае занесения
 * ind - адрес ячейки
 * role - тип заносимых данных в ячейку
 * value - значение
 */
bool GraphMatrixModel::setData(const QModelIndex &ind, const QVariant &value, int role)
{
    Q_UNUSED(role);

    if (!checkIndex(ind))
        return false;

    int row = ind.row();
    int column = ind.column();

    if (row == column)
        return false;

    bool ok;
    uint v = value.toUInt(&ok);
    if (!ok)
        return false;

    if (v == graphMatrix[row][column])
        return false;

    graphMatrix[row][column] = v;
    graphMatrix[column][row] = v;

    QModelIndex mirrorIndex = index(column, row, ind.parent());

    emit dataChanged(ind, ind);
    emit dataChanged(mirrorIndex, mirrorIndex);
    emit matrixChanged();
    return true;
}

/*
 * проверяет корректность адреса ячейки
 * index - адрес ячейки
 */
bool GraphMatrixModel::checkIndex(const QModelIndex &index) const
{
    if (!index.isValid() ||
        index.row() < 0 || index.row() >= graphMatrix.count() ||
        index.column() < 0 || index.column() >= graphMatrix.count())
        return false;

    return true;
}

/*
 * создает матрицу размера size
 */
void GraphMatrixModel::createMatrix(int size)
{
    Q_ASSERT_X(size >= MATRIX_MIN_SIZE, "GraphMatrixModel::createMatrix", "Размер матрицы меньше MATRIX_MIN_SIZE!");
    graphMatrix.clear();

    for (int i = 0; i < size; i++)
        graphMatrix.append(QVector<uint>(size));
}
