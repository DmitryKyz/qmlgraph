#ifndef GRAPHMATRIXMODEL_H
#define GRAPHMATRIXMODEL_H

#include <QAbstractTableModel>
#include <QTextStream>
#include <QtQml/qqml.h>

class GraphMatrixModel : public QAbstractTableModel
{  
    /*
     * Класс модели данных таблицы матрицы смежности
     */

    Q_OBJECT
    QML_ELEMENT

    Q_PROPERTY(QVector<QVector<uint> > matrix READ matrix WRITE setMatrix NOTIFY matrixChanged)
    Q_PROPERTY(int minSize READ minSize CONSTANT)

    constexpr static int NULL_ROLE = Qt::UserRole; // не редактируемые ячейки таблицы

signals:
    void matrixChanged();

public slots:
    void setMatrixSize(int size);

public:
    explicit GraphMatrixModel(QObject *parent=0);

    QVector<QVector<uint> > matrix() const {return graphMatrix;}
    void setMatrix(QVector<QVector<uint>> matrix);
    int minSize();

    void save(QTextStream &out);
    void load(QTextStream &in);

    Qt::ItemFlags flags(const QModelIndex &index) const override;


    QVariant data(const QModelIndex &index,
                  int role=Qt::DisplayRole) const override;
    QVariant headerData(int section, Qt::Orientation orientation,
                        int role=Qt::DisplayRole) const override;
    int rowCount(const QModelIndex &parent=QModelIndex()) const override;
    int columnCount(const QModelIndex &parent=QModelIndex()) const override;

    bool setData(const QModelIndex &ind, const QVariant &value,
                 int role=Qt::EditRole) override;

    bool isNotEditable(const QModelIndex &index) const;

protected:
    QHash<int, QByteArray> roleNames() const override;

private:
    QVector<QVector<uint>> graphMatrix; // матрица смежности

    bool checkIndex(const QModelIndex &index) const;
    void createMatrix(int size);
};

#endif // GRAPHMATRIXMODEL_H
