#include "graphrib.h"
#include "graphvertex.h"

#include <QQmlContext>
#include <QQuickItem>


GraphRib::GraphRib(QSharedPointer<const GraphVertex> beginV, QSharedPointer<const GraphVertex> endV, int weight,
                   QQmlComponent *delegate, QQuickItem *parent):
    QObject(parent), m_beginVertex(beginV), m_endVertex(endV), m_weight(weight)
{
    context = new QQmlContext(delegate->creationContext(), this);
    context->setContextObject(this);
    createItem(delegate);
}

GraphRib::~GraphRib()
{
}

/*
 * Создает QML-элемент на основе делегата
 */
void GraphRib::createItem(QQmlComponent *delegate)
{
    m_item.reset(qobject_cast<QQuickItem*>(delegate->create(context)));

    if (m_item)
        m_item->setParentItem(qobject_cast<QQuickItem*>(parent()));
}

void GraphRib::setWeight(int weight)
{
    if (m_weight != weight)
    {
        m_weight = weight;
        emit weightChanged();
    }
}

void GraphRib::setUse(bool use)
{
    if (m_use != use)
    {
        m_use = use;
        emit useChanged();
    }
}
