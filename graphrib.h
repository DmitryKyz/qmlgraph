#ifndef GRAPHRIB_H
#define GRAPHRIB_H

#include <QObject>
#include "graphvertex.h"
#include <QQuickItem>

class QQmlContext;
class QQuickItem;
class QQmlComponent;
class GraphVertex;


/*
 * Класс ребра графа
 *
 * Создает QML-елемент, является контекстным объектом для него
 */
class GraphRib : public QObject
{
    Q_OBJECT

    Q_PROPERTY(int weight READ weight WRITE setWeight NOTIFY weightChanged)
    Q_PROPERTY(bool use READ use NOTIFY useChanged)

    Q_PROPERTY(QQuickItem* beginVertex READ beginVertexItem CONSTANT)
    Q_PROPERTY(QQuickItem* endVertex READ endVertexItem CONSTANT)

public:
    explicit GraphRib(QSharedPointer<const GraphVertex> beginV, QSharedPointer<const GraphVertex> endV, int weight,
                      QQmlComponent *delegate, QQuickItem *parent = nullptr);
    ~GraphRib();

    int weight() const {return m_weight;}
    bool use() const {return m_use;}

    void setWeight(int weight);
    void setUse(bool use);

    const GraphVertex *beginVertex() const {return m_beginVertex.data();};
    const GraphVertex *endVertex() const {return m_endVertex.data();}

    void createItem(QQmlComponent *delegate);

signals:
    void weightChanged();
    void useChanged();

    void beginVertexChanged();
    void endVertexChanged();

private:
    QSharedPointer<const GraphVertex> m_beginVertex; // начальная вершина
    QSharedPointer<const GraphVertex> m_endVertex; // конечная вершина
    int m_weight = 0; // вес ребра
    bool m_use = false;

    QQmlContext *context = nullptr;
    QScopedPointer<QQuickItem> m_item;

    QQuickItem* beginVertexItem() const {return m_beginVertex->item();}
    QQuickItem* endVertexItem() const {return m_endVertex->item();}
};

#endif // GRAPHRIB_H
