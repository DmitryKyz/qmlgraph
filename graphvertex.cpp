#include "graphvertex.h"
#include <QQmlContext>
#include <QQuickItem>
#include <QPointF>

GraphVertex::GraphVertex(QPointF pos, QString name, int num, QQmlComponent *delegate, QQuickItem *parent):
    QObject(parent), m_name(name), m_num(num)
{
    context = new QQmlContext(delegate->creationContext(), this);
    context->setContextObject(this);

    createItem(delegate);

    if (m_item)
    {
        m_item->setX(pos.x());
        m_item->setY(pos.y());
    }
}

GraphVertex::~GraphVertex()
{
}

/*
 * Создает QML-элемент на основе делегата
 */
void GraphVertex::createItem(QQmlComponent *delegate)
{
    m_item.reset(qobject_cast<QQuickItem*>(delegate->create(context)));

    if (m_item)
        m_item->setParentItem(qobject_cast<QQuickItem*>(parent()));
}

void GraphVertex::setName(QString name)
{
    if (m_name != name)
    {
        m_name = name;
        emit nameChanged();
    }
}

void GraphVertex::setUse(bool use)
{
    if (m_use != use)
    {
        m_use = use;
        emit useChanged();
    }
}

qreal GraphVertex::x() const
{
    if (m_item)
        return m_item->x();
    return 0;
}

qreal GraphVertex::y() const
{
    if (m_item)
        return m_item->y();
    return 0;
}

void GraphVertex::setX(qreal x)
{
    if (m_item)
        m_item->setX(x);
}

void GraphVertex::setY(qreal y)
{
    if (m_item)
        m_item->setY(y);
}
