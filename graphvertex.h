#ifndef GRAPHVERTEX_H
#define GRAPHVERTEX_H

#include <QObject>
#include <QString>
#include <QScopedPointer>

class QQmlContext;
class QQuickItem;
class QQmlComponent;

/*
 * Класс вершины графа
 *
 * Создает QML-елемент, является контекстным объектом для него
 */
class GraphVertex : public QObject
{
    Q_OBJECT

    Q_PROPERTY(QString name READ name WRITE setName NOTIFY nameChanged)
    Q_PROPERTY(bool use READ use NOTIFY useChanged)

signals:
    void nameChanged();
    void useChanged();

public:
    explicit GraphVertex(QPointF pos, QString name, int num, QQmlComponent *delegate, QQuickItem *parent = nullptr);
    ~GraphVertex();

    QString name() const {return m_name;}
    bool use() const {return m_use;}

    QQuickItem *item() const {return m_item.data();}

    qreal x() const;
    qreal y() const;
    void setX(qreal x);
    void setY(qreal y);

    void setName(QString name);
    void setUse(bool use);

    int num() const {return m_num;}

    void createItem(QQmlComponent *delegate);

private:
    QString m_name = "Вершина";
    bool m_use = false;

    QQmlContext *context = nullptr;
    QScopedPointer<QQuickItem> m_item;
    int m_num = -1; // номер вершины в графе
};

#endif // GRAPHVERTEX_H
