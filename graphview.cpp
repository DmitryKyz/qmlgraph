#include "graphview.h"
#include "graphvertex.h"
#include "graphrib.h"
#include "global.h"

#include <QQmlComponent>
#include <QSGGeometryNode>
#include <QSGFlatColorMaterial>
#include <QPointF>
#include <QtMath>
#include <QSharedPointer>


GraphView::GraphView():
    QQuickItem()
{
    setFlag(ItemHasContents, true);
}

QQmlComponent *GraphView::vertexDelegate()
{
    return m_vertexDelegate;
}

QQmlComponent *GraphView::ribDelegate()
{
    return m_ribDelegate;
}

QVector<QVector<uint>> GraphView::matrix()
{
    return graphMatrix;
}

/*
 * Возвращает названия вершин графа
 */
QStringList GraphView::vertexNames()
{
    QStringList list;
    for (auto v : vertex)
        list.append(v->name());

    return list;
}

void GraphView::setVertexDelegate(QQmlComponent *delegate)
{
    if (delegate != m_vertexDelegate)
    {
        m_vertexDelegate = delegate;
        setupVertexItems();
        emit vertexDelegateChanged();
    }
}

void GraphView::setRibDelegate(QQmlComponent *delegate)
{
    if (delegate != m_ribDelegate)
    {
        m_ribDelegate = delegate;
        setupRibItems();
        emit ribDelegateChanged();
    }
}

void GraphView::setMatrix(QVector<QVector<uint>> &matrix)
{
    if (graphMatrix != matrix)
    {
        if (!checkMatrix(matrix))
            return;

        if (graphMatrix.isEmpty())
            createGraph(matrix);
        else if (graphMatrix.size() < matrix.size())
            icreaseGraph(matrix);
        else if (graphMatrix.size() > matrix.size())
            reduceGraph(matrix);
        else
            changeRibWeight(matrix);

        sentDataToAlgorithm();
        emit matrixChanged();
    }
}

/*
 * создание нового графа
 *
 * matrix - новая матрица смежности
 */
void GraphView::createGraph(QVector<QVector<uint>> &matrix)
{
    graphMatrix = matrix;
    clearGraph();

    QPointF beginPos(200, 100);
    QPointF stepPos(100, 0); // смещение позиции
    float angle = 360.0 / matrix.count();

    // создание вершин
    if (m_vertexDelegate)
    {
        for (int i = 0; i < matrix.count(); i++)
        {
            QPointF newPoint = beginPos + stepPos; // позиция вершины
            newPoint = rotatePoint(beginPos, newPoint, angle * i); // поворот позиции вокруг предыдущей вершины
            vertex.append(QSharedPointer<GraphVertex>::create(newPoint, getVertexName(i), i, m_vertexDelegate, this));
            beginPos = newPoint;
        }
    }

    // создание ребер
    if (m_ribDelegate)
    {
        for (int i = 0; i < graphMatrix.count(); i++)
            for (int j = i; j < graphMatrix.count(); j++)
                if (graphMatrix[i][j] > 0)
                    ribs.append(QSharedPointer<GraphRib>::create(vertex[i], vertex[j], graphMatrix[i][j], m_ribDelegate, this));
    }
}

/*
 * обработка увеличения количества вершин в графе
 *
 * matrix - новая матрица смежности
 */
void GraphView::icreaseGraph(QVector<QVector<uint>> &matrix)
{
    createGraph(matrix);
}

/*
 * обработка уменьшения количества вершин в графе
 *
 * matrix - новая матрица смежности
 */
void GraphView::reduceGraph(QVector<QVector<uint>> &matrix)
{
    int newSize = matrix.size();

    for (int i = ribs.count()-1; i >= 0; i--)
    {
        if (ribs[i]->beginVertex()->num() >= newSize ||
                ribs[i]->endVertex()->num() >= newSize)
            ribs.removeAt(i);
    }

    while (vertex.count() > newSize)
        vertex.removeLast();

    changeRibWeight(matrix);
}

/*
 * обработка изменения веса ребер
 *
 * matrix - новая матрица смежности
 */
void GraphView::changeRibWeight(QVector<QVector<uint>> &matrix)
{
    // задание веса ребрам
    for (auto r : ribs)
    {
        int bIndex = r->beginVertex()->num();
        int eIndex = r->endVertex()->num();

        if (graphMatrix[bIndex][eIndex] != matrix[bIndex][eIndex])
            r->setWeight(matrix[bIndex][eIndex]);
    }

    // создание ребер, у которых вес стал больше нуля
    for (int i = 0; i < matrix.count(); i++)
        for (int j = i; j < matrix.count(); j++)
            if (graphMatrix[i][j] == 0 && matrix[i][j] > 0)
                ribs.append(QSharedPointer<GraphRib>::create(vertex[i], vertex[j], matrix[i][j], m_ribDelegate, this));

    // удаление ребер у которых вес стал равен нулю
    for (int i = ribs.count()-1; i >= 0; i--)
    {
        if (ribs[i]->weight() == 0)
            ribs.removeAt(i);
    }

    graphMatrix = matrix;
}

void GraphView::setupVertexItems()
{
    for (auto v : vertex)
        v->createItem(m_vertexDelegate);
}

void GraphView::setupRibItems()
{
    for (auto r : ribs)
        r->createItem(m_ribDelegate);
}

void GraphView::setAlgorithm(GraphAlgorithm *algorithm)
{
    if (m_algorithm != algorithm)
    {
        m_algorithm = algorithm;
        sentDataToAlgorithm();
        algorithmChanged();
    }
}

void GraphView::sentDataToAlgorithm()
{
    if (!m_algorithm)
        return;

    m_algorithm->setRibs(ribs);
    m_algorithm->setVertex(vertex);
}

/*
 * Удаляет ребра и вершины в графе.
 */
void GraphView::clearGraph()
{
    //for (auto r : ribs)
    //    delete r;
    ribs.clear();

    //for (auto v : vertex)
    //    delete v;
    vertex.clear();

    sentDataToAlgorithm();
}

/*
 * Проверяет корректность матрицы (является ли она квадратной)
 * matrix - матрица смежности неориентированного взвешенного графа
 */
bool GraphView::checkMatrix(QVector<QVector<uint> > &matrix)
{

    int size = matrix.count();
    if (size < MATRIX_MIN_SIZE)
        return false;

    for (int i = 0; i < size; i++)
        if (matrix[i].count() != size)
            return false;

    for (int i = 0; i < matrix.count(); i++)
        for (int j = 0; j < matrix.count(); j++)
        {
            if (i == j && matrix[i][j] != 0)
                return false;

            if (matrix[i][j] != matrix[j][i])
                return false;
        }

    return true;
}

/*
 * Передвигает граф внутри представления на позицию (x; y)
 */
void GraphView::moveBy(int x, int y)
{
    if (x || y)
    {
        for (auto v : vertex)
        {
            v->setX(v->x() + x);
            v->setY(v->y() + y);
        }
    }
}

/*
 * Поворачивает позицию point вокруг rotPoint на угол angle
 */
QPointF GraphView::rotatePoint(QPointF rotPoint, QPointF point, float angle)
{
    point -= rotPoint;

    float x = point.x();
    float y = point.y();

    float rad = qDegreesToRadians(angle);

    float x1 = x*qCos(rad) - y*qSin(rad);
    float y1 = x*qSin(rad) + y*qCos(rad);

    QPointF newPoint(x1, y1);

    newPoint += rotPoint;
    return newPoint;
}
