#ifndef GRAPHVIEW_H
#define GRAPHVIEW_H

#include <QQuickItem>
#include "graphalgorithm.h"

class GraphRib;
class GraphVertex;

/*
 * Класс представления графа
 *
 * является родительским для всех элементов графа
 */
class GraphView : public QQuickItem
{
    Q_OBJECT
    QML_ELEMENT

    Q_PROPERTY(QQmlComponent *vertexDelegate READ vertexDelegate WRITE setVertexDelegate NOTIFY vertexDelegateChanged)
    Q_PROPERTY(QQmlComponent *ribDelegate READ ribDelegate WRITE setRibDelegate NOTIFY ribDelegateChanged)
    Q_PROPERTY(QVector<QVector<uint>> matrix READ matrix WRITE setMatrix NOTIFY matrixChanged)
    Q_PROPERTY(GraphAlgorithm *algorithm READ algorithm WRITE setAlgorithm NOTIFY algorithmChanged)
    Q_PROPERTY(QStringList vertexNames READ vertexNames NOTIFY matrixChanged)

public:
    GraphView();

    QQmlComponent *vertexDelegate();
    QQmlComponent *ribDelegate();
    QVector<QVector<uint>> matrix();
    GraphAlgorithm *algorithm() {return m_algorithm;}
    QStringList vertexNames();

    void setVertexDelegate(QQmlComponent *delegate);
    void setRibDelegate(QQmlComponent *delegate);
    void setMatrix(QVector<QVector<uint> > &matrix);
    void setAlgorithm(GraphAlgorithm *algorithm);

public slots:
    void moveBy(int x, int y);

private:
    QList<QSharedPointer<GraphRib>> ribs; // список ребер графа
    QVector<QSharedPointer<GraphVertex>> vertex; // вектор вершин графа
    QVector<QVector<uint>> graphMatrix;

    QQmlComponent *m_vertexDelegate = nullptr;
    QQmlComponent *m_ribDelegate = nullptr;

    GraphAlgorithm *m_algorithm = nullptr;

    void clearGraph();
    bool checkMatrix(QVector<QVector<uint> > &matrix);
    QPointF rotatePoint(QPointF rotPoint, QPointF point, float angle);
    void sentDataToAlgorithm();

    void setupVertexItems();
    void setupRibItems();

    void createGraph(QVector<QVector<uint>> &matrix);
    void icreaseGraph(QVector<QVector<uint>> &matrix);
    void reduceGraph(QVector<QVector<uint>> &matrix);
    void changeRibWeight(QVector<QVector<uint>> &matrix);

signals:
    void vertexDelegateChanged();
    void ribDelegateChanged();
    void matrixChanged();
    void algorithmChanged();
};

#endif // GRAPHVIEW_H
