import QtQuick 2.15
import QtQuick.Window 2.12
import QtQuick.Layouts 1.3
import QtQuick.Controls 2.15
import Qt.labs.platform 1.0
import graph 1.0

Window {

    id: root

    width: 500
    height: 500

    Item {
       id: item
       anchors.fill: parent

       ColumnLayout
       {
           anchors.fill: parent
           spacing: 1
           Rectangle {

               Layout.fillHeight: true
               Layout.fillWidth: true

               color: "lightGrey"
               border.color: "#0f0e0e"

               GraphView
               {
                   id: graphView

                   vertexDelegate: vertex
                   ribDelegate: rib
                   algorithm: algorithm

                   matrix: model.matrix
                   anchors.fill: parent


                   MouseArea {

                       property point mousePos

                       anchors.fill: parent
                       onPressed: {
                           mousePos = Qt.point(mouseX, mouseY)
                           parent.focus = true
                       }

                       onPositionChanged: {
                           parent.moveBy(mouseX - mousePos.x, mouseY - mousePos.y)
                           mousePos = Qt.point(mouseX, mouseY)
                       }
                   }
               }
           }

           Rectangle {

               Layout.fillWidth: true
               Layout.preferredHeight: 50
               color: "#2d2929"
               border.color: "black"

               RowLayout{

                   anchors.fill: parent
                   ComboBox {
                       id: comboBox

                       Layout.margins: 5
                       Layout.alignment: Qt.AlignVCenter
                       Layout.preferredWidth: 200

                       model: ["Минимальное остовное дерево", "Кратчайший путь"]
                   }

                   Button
                   {
                       id: processBtn
                       Layout.alignment: Qt.AlignVCenter
                       text: qsTr("Выполнить")
                       onClicked: {
                           switch(comboBox.currentIndex)
                           {
                           case 0:
                               processMinimalSpanningTree()
                               break
                           case 1:
                               processMinPath()
                               break
                           default:
                               resultText.text = qsTr("Не могу выполнить алгоритм!")
                           }
                       }
                   }

                   Button
                   {
                       id: resetBtn
                       Layout.alignment: Qt.AlignVCenter
                       text: qsTr("Сброс")
                       onClicked: {
                           algorithm.reset()
                           resultText.text = ""
                       }
                   }

                   Text {
                       id: resultText

                       Layout.alignment: Qt.AlignVCenter
                       Layout.fillWidth: true

                       //text: qsTr("")
                       color: "white"
                       font.pixelSize: 14
                   }

                   ToolButton
                   {
                       id: saveImgBtn

                       Layout.margins: 5
                       Layout.alignment: Qt.AlignVCenter | Qt.AlignRight

                       text: qsTr("Сохранить")

                       icon.source: "icons/Save.png"
                       icon.height: 32
                       icon.width: 32

                       display: AbstractButton.IconOnly

                       onClicked: {
                           saveDialog.visible = true
                       }
                   }
               }
           }
       }
    }

    FileDialog {
        id: saveDialog

        title: qsTr("Экспорт в изображение")
        nameFilters: [ "PNG-файл (*.png)", "JPEG-файл (*.jpg)" ]
        fileMode: FileDialog.SaveFile

        onAccepted: {
            console.log("save", graphIO.fromUrl(file))

            item.grabToImage(function(result) {
                                result.saveToFile(graphIO.fromUrl(file));
                               });
        }
    }

    GraphIO
    {
        id: graphIO
    }

    Component
    {
        id: vertex

        Rectangle
        {
            width: 40
            height: width
            z: 1

            radius: width / 2
            color: getColor(focus, use)

            border.color: "black"

            Text {
                text: name
                anchors.centerIn: parent
            }

            MouseArea {
                anchors.fill: parent
                drag.target: parent

                onPressed: parent.focus = true
            }
        }
    }

    Component
    {
        id: rib

        GraphLine
        {
            x1: beginVertex.x + beginVertex.width / 2
            y1: beginVertex.y + beginVertex.height / 2
            x2: endVertex.x + endVertex.width / 2
            y2: endVertex.y + endVertex.height / 2
            z: 0

            lineWidth: 3
            color: use ? "magenta" : "green"

            Rectangle
            {
                anchors.centerIn: parent

                width: weightText.text > 9 ? weightText.width * 2 : weightText.width * 3
                height: width
                radius: width / 2

                color: "#0d8fe0"
                border.color: "#1e1c1b"

                Text {
                    id: weightText
                    anchors.centerIn: parent
                    text: weight
                    font.pixelSize: 12
                }
            }
        }
    }

    GraphAlgorithm
    {
        id: algorithm
    }

    function getColor(focus, isUse)
    {
        if (focus)
            return "green"

        if (isUse)
            return "magenta"

        return "orange"
    }

    function processMinimalSpanningTree()
    {
        var result = algorithm.processMinimalSpanningTree()
        resultText.text = result["message"]
    }

    function processMinPath()
    {
        pathDialog.vertexNames = graphView.vertexNames
        pathDialog.visible = true
    }

    PathDialog
    {
        id: pathDialog
        onAccepted: {
            var result = algorithm.processMinimalPath(beginPath, endPath)
            resultText.text = result["message"]
        }
    }
}
