#include <QGuiApplication>
#include <QQmlApplicationEngine>
#include <QQuickWindow>
#include <QQuickStyle>

#include "graphmatrixmodel.h"
#include "graphview.h"
#include "graphline.h"
#include "graphio.h"
#include "graphalgorithm.h"

int main(int argc, char *argv[])
{
    QCoreApplication::setAttribute(Qt::AA_EnableHighDpiScaling);

    QGuiApplication app(argc, argv);

    app.setOrganizationName("Kyz");
    app.setOrganizationDomain("Kyz.com");
    app.setApplicationName("QMLGraph");

    QQmlApplicationEngine engine;
    const QUrl url(QStringLiteral("qrc:/main.qml"));
    QObject::connect(&engine, &QQmlApplicationEngine::objectCreated,
                     &app, [url](QObject *obj, const QUrl &objUrl) {
        if (!obj && url == objUrl)
            QCoreApplication::exit(-1);
    }, Qt::QueuedConnection);

    QQuickStyle::setStyle("Fusion");
    engine.load(url);

    return app.exec();
}
