import QtQuick 2.12
import QtQuick.Window 2.12
import QtQuick.Controls 2.15
import Qt.labs.qmlmodels 1.0
import QtQuick.Layouts 1.12
import Qt.labs.platform 1.0

import graph 1.0

ApplicationWindow {
    id: root
    visible: true
    width: 640
    height: 480
    title: qsTr("Задание графа")

    //color: "black"
    font.pixelSize: 14

    property variant win
    property variant fileFilter: [ "Матрица смежности (*.mtx)" ]

    header: ToolBar
    {
        id: toolBar

        Flow
        {
            anchors.fill: parent

            ToolButton
            {
                id: saveBtn

                text: qsTr("Сохранить")

                icon.source: "icons/Save.png"
                icon.height: 32
                icon.width: 32

                display: AbstractButton.IconOnly

                onClicked: saveDialog.visible = true
            }

            ToolButton
            {
                id: loadBtn

                text: qsTr("Загрузить")

                icon.source: "icons/Open.png"
                icon.height: 32
                icon.width: 32

                display: AbstractButton.IconOnly

                onClicked: loadDialog.visible = true
            }
        }
    }

    ColumnLayout
    {
        id: layout
        Layout.margins: 10

        anchors.fill: root.contentItem

        GraphTable
        {
            Layout.fillHeight: true
            Layout.fillWidth: true

            model: model
            delegate: delegate
        }

        RowLayout
        {
            id: rowL
            Layout.margins: 10
            spacing: 5
            Label
            {
                text: qsTr("Количество вершин:")
            }

            SpinBox
            {
                id: spinBox
                Layout.preferredWidth: 50
                from: model.minSize
                to: 15
                value: model.rowCount()
                onValueChanged: model.setMatrixSize(value)
            }
        }

        Button
        {
            id: botton
            Layout.alignment: Qt.AlignCenter
            Layout.margins: 5
            text: qsTr("Построить граф")
            onClicked: {
                if (!win)
                {
                    var component = Qt.createComponent("GraphWindow.qml");
                    win = component.createObject(root);
                }
                win.show();
            }

        }
    }

    GraphMatrixModel
    {
        id: model
    }

    Component {
        id: delegate

        SpinBoxDelegate {
            displayValue: display
            editable: isNull ? false : true
        }
    }

    GraphIO
    {
        id: graghIO

        onLoaded: {
            model.matrix = graghIO.matrix
            spinBox.value = model.rowCount()
        }

        onErrorChanged: {
            errorMessage.visible = true
            errorMessage.text = error
        }
    }


    FileDialog {
        id: saveDialog

        title: qsTr("Сохранить матрицу")
        nameFilters: fileFilter
        fileMode: FileDialog.SaveFile

        onAccepted: {
            graghIO.source = file
            graghIO.matrix = model.matrix
            graghIO.save()
        }
    }

    FileDialog {
        id: loadDialog

        title: qsTr("Загрузить матрицу")
        nameFilters: fileFilter

        onAccepted: {
            graghIO.source = file
            graghIO.load()
        }
    }

    Message
    {
        id: errorMessage
    }
}


