import QtQuick 2.0
import QtQuick.Window 2.12
import QtQuick.Layouts 1.3
import QtQuick.Controls 2.15
import QtQuick.Dialogs 1.2


Dialog {
    id: root
    title: qsTr("Выбрать путь")

    property var vertexNames: []
    property alias beginPath: beginPathCB.currentText
    property alias endPath: endPathCB.currentText

    standardButtons: StandardButton.Ok | StandardButton.Cancel

    ColumnLayout
    {
        anchors.fill: parent
        spacing: 1

        Text
        {
            Layout.maximumHeight: 20
            Layout.fillWidth: true
            Layout.alignment: Qt.AlignTop

            text: qsTr("Выберите вершины путь к которым необходимо найти")
            font.pixelSize: 14
        }

        GridLayout{
            columns: 2
            rows: 2

            Layout.alignment: Qt.AlignTop

            Text
            {         
                text: qsTr("Начало пути:")
                font.pixelSize: 14

                Layout.alignment: Qt.AlignTop
            }

            ComboBox {
                id: beginPathCB
                model: vertexNames

                Layout.alignment: Qt.AlignTop
            }

            Text
            {
                text: qsTr("Конец пути:")
                font.pixelSize: 14

                Layout.alignment: Qt.AlignTop
            }

            ComboBox {
                id: endPathCB
                model: vertexNames

                Layout.alignment: Qt.AlignTop
            }
        }
    }
}
