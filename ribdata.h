#ifndef RIBDATA_H
#define RIBDATA_H

/*
 * RibData - хранит данные о ребре графа
 */

struct RibData
{
    int start; // начальная вершина
    int end; // конечная вершина
    int weight; // вес ребра
};

#endif // RIBDATA_H
